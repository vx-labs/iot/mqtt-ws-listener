FROM vxlabs/glide:1.10 as builder

RUN mkdir -p $GOPATH/src/github.com/vx-labs
WORKDIR $GOPATH/src/github.com/vx-labs/iot-mqtt-ws-listener
RUN mkdir release
COPY glide* ./
RUN glide install
COPY . ./
RUN go test $(glide nv) && \
    go build -buildmode=exe -a -o /bin/listener ./cmd/server

FROM alpine
EXPOSE 8883
ENTRYPOINT ["/usr/bin/server"]
RUN apk -U add ca-certificates && \
    rm -rf /var/cache/apk/*
COPY --from=builder /bin/listener /usr/bin/server

