package listener

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"sync"
	"time"

	"net/http"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/sirupsen/logrus"
	broker "github.com/vx-labs/iot-mqtt-broker/api"
	api "github.com/vx-labs/iot-mqtt-broker/listener"
	"google.golang.org/grpc"
)

type Listener struct {
	listener   io.Closer
	logger     *logrus.Entry
	routines   sync.WaitGroup
	broker     *broker.Client
	brokerConn io.Closer
}

func NewListener(ctx context.Context) *Listener {
	l := &Listener{
		logger:   logrus.WithField("source", "listener"),
		routines: sync.WaitGroup{},
	}

	l.connectToBroker()
	return l
}

func (l *Listener) connectToBroker() error {
	conn, err := grpc.Dial(os.Getenv("BROKER_HOST"), broker.DefaultGRPCOpts()...)
	if err != nil {
		return err
	}
	l.brokerConn = conn
	l.broker = broker.NewClient(conn)
	return nil
}

func (h *Listener) Close() error {
	err := h.listener.Close()
	if err != nil {
		return err
	}
	h.routines.Wait()
	logrus.Infof("listener stopped")
	return nil
}

type Conn struct {
	conn      net.Conn
	reader    *wsutil.Reader
	writer    *wsutil.Writer
	opHandler wsutil.FrameHandler
}

func (c *Conn) Read(b []byte) (int, error) {
	n, err := c.reader.Read(b)
	if err == io.EOF || err == wsutil.ErrNoFrameAdvance {
		for {
			header, err := c.reader.NextFrame()
			if err != nil {
				return n, err
			}
			if header.OpCode.IsData() {
				return c.Read(b)
			}
			if header.OpCode.IsControl() {
				if err = c.opHandler(header, c.reader); err != nil {
					log.Printf("websocket control op handler failed: %v", err)
					return n, io.EOF
				}
			}
		}
	}

	return n, err
}
func (c *Conn) Write(b []byte) (int, error) {
	n, err := c.writer.Write(b)
	if err == nil {
		return n, c.writer.Flush()
	}
	return n, err
}

func (c *Conn) Close() error {
	return c.conn.Close()
}

func (c *Conn) SetDeadline(t time.Time) error {
	return c.conn.SetDeadline(t)
}
func (h *Listener) StartWS(port int) {
	h.routines.Add(1)
	defer func() {
		h.routines.Done()
		logrus.Infof("WS listener stopped")
	}()

	mux := http.NewServeMux()
	mux.HandleFunc("/mqtt", func(w http.ResponseWriter, r *http.Request) {
		h.logger.Infof("starting websocket negociation with %s", r.RemoteAddr)
		conn, _, _, err := ws.UpgradeHTTP(r, w, http.Header{
			"Sec-WebSocket-Protocol": {"mqtt"},
		})
		if err != nil {
			h.logger.Errorf("websocket negociation with %s failed: %v", r.RemoteAddr, err)
			return
		}

		var (
			state  = ws.StateServerSide
			reader = wsutil.NewReader(conn, state)
			writer = wsutil.NewWriter(conn, state, ws.OpBinary)
		)
		h.connManager(&Conn{
			conn:      conn,
			reader:    reader,
			writer:    writer,
			opHandler: wsutil.ControlHandler(conn, state),
		})
		return

		for {
			header, err := reader.NextFrame()
			if err != nil {
				// handle error
			}

			// Reset writer to write frame with right operation code.
			writer.Reset(conn, state, header.OpCode)

			if _, err = io.Copy(writer, reader); err != nil {
				// handle error
			}

			if err = writer.Flush(); err != nil {
				// handle error
			}
		}
	})
	l := &http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%d", port),
		Handler: mux,
	}
	h.listener = l
	l.ListenAndServe()
}

func (h *Listener) connManager(conn *Conn) {
	transport := api.Transport{
		Channel:       conn,
		Encrypted:     true,
		Protocol:      "wss",
		RemoteAddress: conn.conn.RemoteAddr().String(),
	}

	logrus.Infof("accepted new connection from %s", transport.RemoteAddress)
	err := api.Run(h.broker, transport)
	if err != nil {
		logrus.Errorf("failure in connection from %s: %v", transport.RemoteAddress, err)
	} else {
		logrus.Infof("terminated connection from %s", transport.RemoteAddress)
	}
}
