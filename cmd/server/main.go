package main

import (
	"context"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/vx-labs/iot-mqtt-ws-listener/listener"
	"github.com/vx-labs/iot-mqtt-ws-listener/metrics"
)

func main() {

	logger := logrus.New()
	if os.Getenv("API_ENABLE_PROFILING") == "true" {
		go func() {
			logger.Println(http.ListenAndServe(":8080", nil))
		}()
	}
	m := metrics.NewMetricHandler()
	mqtt := listener.NewListener(context.Background())
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	go func() {
		mqtt.StartWS(8008)
	}()
	logger.Infof("server is running on :8008")
	<-sigc
	logger.Infof("received interruption: closing broker")
	mqtt.Close()
	m.Close()
}
